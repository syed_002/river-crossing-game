# Space obstacle Game
Welcome To The Game

    To play this game: Install pygame and run 'python ggame.py' in terminal

1. If the Player(Bot) crosses an Asteroid, then he gains 5 points and if crosses an Alien , gains 10 points 
2. The Bot starts from left most corner of the display and if collides to the fixed obstacle i.e., Asteroid, score decreases by 5 points and if collides to moving obstacle i.e.,Aliens, score decreases by 10 points
3. If Player_1 reaches the END, its score is stored. It then becomes Player_2 and then reaches to the other end. The scores are compared and the winner is displayed.
4. The speed of the Aliens increases after each round

 